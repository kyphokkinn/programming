package com.example.phokkinnky.programming;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by phokkinnky on 2/13/2017.
 */

public class LessonAdapter2 extends RecyclerView.Adapter<LessonAdapter2.MyViewHolder>{
    private Context context;
    private MyDatabasePro myDatabasePro;
    private static boolean fav;
    public interface onItemClickListener{
        void onLessonClick(Lesson lesson);
    }
    private  List<Lesson> lessons;
    private  onItemClickListener listener;
    public LessonAdapter2(List<Lesson> lessons, onItemClickListener listener,Context context) {
        this.lessons = lessons;
        this.listener = listener;
        this.context=context;

    }

    @Override
    public LessonAdapter2.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_by_lesson,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final LessonAdapter2.MyViewHolder holder, final int position) {
        //holder.title.setText(lessons.get(position).getTitlelesson());
        myDatabasePro = new MyDatabasePro(context);
        holder.bind(lessons.get(position),listener);
        holder.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(fav==false){
                    myDatabasePro.getWritableDatabase().rawQuery("INSERT INTO tbfavorite VALUES("
                            +lessons.get(position).getLessonID()+","+
                            lessons.get(position).getTitlelesson()+","+
                            lessons.get(position).isLike()+")",null);
                holder.favorite.setImageResource(R.drawable.ic_favorite_black_24dp);
                    fav =true;
                }

                else {
                    String[] select={lessons.get(position).getLessonID()+"%"};
                    myDatabasePro.getWritableDatabase().rawQuery("DELETE FROM tbfavorite WHERE ID LIKE ?",select);
                    holder.favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                fav = false; }

            }
        });
    }

    @Override
    public int getItemCount() {
        return lessons.size();
    }
    public static class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView title;
        public ImageView favorite;
        //public ImageView imageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.lesson);
            favorite = (ImageView) itemView.findViewById(R.id.like);

            //imageView= (ImageView) itemView.findViewById(R.id.like);
        }
        public void bind (final Lesson lesson,final onItemClickListener listener)
        {
            title.setText(lesson.getTitlelesson());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onLessonClick(lesson);
                }
            });

        }

    }
}
