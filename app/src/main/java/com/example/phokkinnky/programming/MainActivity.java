package com.example.phokkinnky.programming;

import android.app.ProgressDialog;
import android.icu.text.LocaleDisplayNames;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Course> courses;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        courses = new ArrayList<>();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview_main);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));
        //CouresAdapter couresAdapter = new CouresAdapter(getDummyData(),getBaseContext());
        //recyclerView.setAdapter(couresAdapter);
    }

    private List<Course> getDummyData() {
       /* courses.add(new Course(R.drawable.c_cpro));
        courses.add(new Course(R.drawable.c_cplus));
        courses.add(new Course(R.drawable.c_csharp));
        courses.add(new Course(R.drawable.c_datastructure));
        courses.add(new Course(R.drawable.c_java));
        courses.add(new Course(R.drawable.c_javascript));
        courses.add(new Course(R.drawable.c_asp));
        courses.add(new Course(R.drawable.c_php));
        courses.add(new Course(R.drawable.c_html));
        courses.add(new Course(R.drawable.c_jquery));
        courses.add(new Course(R.drawable.c_access));
        courses.add(new Course(R.drawable.c_word));
        courses.add(new Course(R.drawable.c_excel));
        courses.add(new Course(R.drawable.c_powerpoint));
        */
        return courses;
    }


}
