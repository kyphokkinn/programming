package com.example.phokkinnky.programming;

import android.database.sqlite.SQLiteOpenHelper;


        import android.content.ContentValues;
        import android.content.Context;
        import android.database.Cursor;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;
        import android.util.Log;

        import java.util.ArrayList;

/**
 * Created by phokkinnky on 2/18/2017.
 */

public class Database extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "dbFavorite";

    // Contacts table name
    private static final String TABLE_NAME = "tbFav";

    // Contacts Table Columns names
    private static final String KEY_ID = "ID";
    private static final String KEY_NAME = "TITLE";
    private static final String KEY_LIKE="LIKE";
    private final ArrayList<Lesson> lessonArrayList= new ArrayList<Lesson>();

    public Database(Context context) {
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE_NAME + "( "+KEY_ID + " TEXT,"+KEY_NAME+" TEXT,"+KEY_LIKE+" INTEGER" + ");";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        // Create tables again
        onCreate(db);
    }
    public void AddFav(Lesson lesson){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues v = new ContentValues();
        v.put(KEY_ID,lesson.getLessonID());
        v.put(KEY_NAME,lesson.getTitlelesson());
        v.put(KEY_LIKE,lesson.isLike());
        db.insert(TABLE_NAME,null,v);
        db.close();
    }
    public ArrayList<Lesson> getAllFav(){
        try {
            lessonArrayList.clear();

            String sql = "SELECT  * FROM " + TABLE_NAME +" WHERE " + KEY_LIKE +" = 1";
            SQLiteDatabase db = getWritableDatabase();
            Cursor cursor = db.rawQuery(sql, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Lesson lesson = new Lesson(cursor.getString(0), cursor.getString(1), Integer.parseInt(cursor.getString(2)));
                    lessonArrayList.add(lesson);
                } while (cursor.moveToNext());
            }
            cursor.close();
            db.close();

        }catch (Exception e){
            Log.e("ALL Lesson",""+e);
        }
        return lessonArrayList;
    }
    public void UpdateLesson(Lesson lesson) {
        SQLiteDatabase db = this.getWritableDatabase();
        // updating row
        String sqlupdate = "UPDATE "+TABLE_NAME + " SET "+KEY_LIKE+ " = "+ lesson.isLike() +" WHERE " + KEY_ID +" = "+lesson.getLessonID()+";";
        db.execSQL(sqlupdate);

    }

    // Deleting single contact
    public void Delete_Fave(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, KEY_ID + " = ?",
                new String[] { id });
        db.close();
    }



}