package com.example.phokkinnky.programming;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by phokkinnky on 1/21/2017.
 */

public class Course {
    public String id;

    public Bitmap urlImage;


    public String getCourseName() {
        return courseName;
    }

    public String courseName;

    public Bitmap getUrlImage() {
        return urlImage;
    }

    public String getId() {
        return id;
    }

    public Course(String id, String name , Bitmap urlImage) {
        this.courseName = name;
        this.id=id;
        this.urlImage = urlImage;
    }
}
