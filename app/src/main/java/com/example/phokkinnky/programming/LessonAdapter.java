package com.example.phokkinnky.programming;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by phokkinnky on 2/5/2017.
 */

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.MyViewHolder>{
    private List<Lesson> lessons = new LinkedList<>();
    private LessonAdapterLisenter lessonAdapterLisenter ;
    private Context context;
    private Cursor cursorLesson;

    public void setLessonAdapterLisenter (LessonAdapterLisenter lessonAdapterLisenter){
        this.lessonAdapterLisenter=lessonAdapterLisenter;
    }
    public LessonAdapter(Cursor cursor, Context context) {
        this.cursorLesson=cursor;
        this.context = context;
    }
    public void setCursorLesson(Cursor cursorLesson){
        this.cursorLesson=cursorLesson;
        notifyDataSetChanged();
    }
    public void appendToList(List<Lesson> list) {
        if (list == null) {
            return;
        }
        lessons.addAll(list);
    }

    public void clear() {
        lessons.clear();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_by_lesson,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        cursorLesson.moveToPosition(position);
        String id= cursorLesson.getString(0);
        String title = cursorLesson.getString(1);
        Integer like = cursorLesson.getInt(2);
        holder.title.setText(title);
    }

    @Override
    public int getItemCount() {
        return cursorLesson.getCount();
    }
    public  class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView title;
        public MyViewHolder(final View view)
        {
            super(view);
            title = (TextView) view.findViewById(R.id.lesson);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getLayoutPosition();
                    cursorLesson.moveToPosition(position);
                    String id = cursorLesson.getString(0);
                    String title= cursorLesson.getString(1);
                    Integer like = cursorLesson.getInt(2);
                    Lesson lesson = new Lesson(id,title,like);
                    lessonAdapterLisenter.onLessonClick(lesson);
                }
            });
        }
    }
}
