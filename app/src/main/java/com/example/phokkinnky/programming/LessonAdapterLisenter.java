package com.example.phokkinnky.programming;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

/**
 * Created by phokkinnky on 2/12/2017.
 */

public interface LessonAdapterLisenter {
    public void onLessonClick(Lesson lesson);
}
