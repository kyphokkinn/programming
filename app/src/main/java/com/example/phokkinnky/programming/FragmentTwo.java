package com.example.phokkinnky.programming;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by phokkinnky on 2/5/2017.
 */
public class FragmentTwo extends Fragment{
    @BindView(R.id.recyclerview_favorite) RecyclerView recyclerView;
    private View view;
    private MyDatabasePro myDatabasePro;
    private Cursor cursor;
    private LessonAdapter lessonAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view= inflater.inflate(R.layout.recycler_lesson,container,false);
        ButterKnife.bind(this,view);

        myDatabasePro = new MyDatabasePro(getContext());
        //String[] selectArgs={"%"};
        Cursor cursor = myDatabasePro.getReadableDatabase().rawQuery("SELECT * FROM tbfavorite WHERE LOVE = 1",null);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),1));
        lessonAdapter = new LessonAdapter(cursor,getContext());
        recyclerView.setAdapter(lessonAdapter);
        lessonAdapter.setLessonAdapterLisenter(new LessonAdapterLisenter() {
            @Override
            public void onLessonClick(Lesson lesson) {
                Intent intent = new Intent(getContext(),DetailsActivity.class);
                intent.putExtra("ID",lesson.getLessonID());
                intent.putExtra("TITLE",lesson.getTitlelesson());
                intent.putExtra("LIKEORHIS",lesson.isLike());
                startActivity(intent);
            }
        });
        return view;
    }
//    private List<Lesson> getDummy()
//    {
//        Database database=new Database(getContext());
////        lessonArrayList.add(new Lesson("001", "Chapter one",0));
////        lessonArrayList.add(new Lesson("001", "Chapter two",0));
////        lessonArrayList.add(new Lesson("001", "Chapter three",0));
////        lessonArrayList.add(new Lesson("001", "Chapter four",0));
////        lessonArrayList.add(new Lesson("001", "Chapter five",0));
////        lessonArrayList.add(new Lesson("001", "Chapter six",0));
////        lessonArrayList.add(new Lesson("001", "Chapter seven",0));
//        return lessonArrayList=database.getAllFav();
//    }

}
