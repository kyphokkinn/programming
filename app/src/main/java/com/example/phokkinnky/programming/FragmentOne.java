package com.example.phokkinnky.programming;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.DrawableWrapper;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatCallback;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.LoginFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by phokkinnky on 2/5/2017.
 */
public class FragmentOne extends Fragment implements CouresAdapterLinstener {

    @BindView( R.id.recyclerview_home) RecyclerView homeRecyclerView;
    private ArrayList<Course> courses;
    private View view ;
    private CouresAdapter adapter;
    private JSONArray user;
    private ArrayList<String> courseID;
    private  ArrayList<String> courseName;
    private  ArrayList<String> chapterID;
    private  ArrayList<String> chapterName;
    private String currentCourseID;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.recycler_view,container,false);
        ButterKnife.bind(this,view);

        homeRecyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        adapter = new CouresAdapter(getContext(),this);
        homeRecyclerView.setAdapter(adapter);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);



    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        courseID = new ArrayList<>();
        courseName = new ArrayList<>();
        courses = new ArrayList<>();
        chapterID = new ArrayList<>();
        chapterName =  new ArrayList<>();
        String username = "testuser";
        String password = "testpass";
        new PostAsync().execute(username, password);

        //activity = (HomeActivity) context;
    }
    private List<Course> getDummyData() {

        //for (int i =0; i<=13; i++) {

        Log.d("getDummyData", "Am I created?");

        /*  courses.add(new Course("C2", R.drawable.rsz_ic_c));
         courses.add(new Course("C3", R.drawable.rsz_ic_c));
         courses.add(new Course("C4", R.drawable.rsz_ic_c));
         courses.add(new Course("C5", R.drawable.rsz_ic_c));
         courses.add(new Course("C6", R.drawable.rsz_ic_c));
         courses.add(new Course("C7", R.drawable.rsz_ic_c));
         courses.add(new Course("C8", R.drawable.rsz_ic_c));
         courses.add(new Course("C9", R.drawable.rsz_ic_c));
         courses.add(new Course("C10", R.drawable.rsz_ic_c));
         courses.add(new Course("C11", R.drawable.rsz_ic_c));
         courses.add(new Course("C12", R.drawable.rsz_ic_c)); */

       // }


        return courses;

    }
    @Override
    public void onCourseClick(Course course) {


        Intent intent = new Intent(getContext(), LessonActivity.class);
        intent.putExtra("ID", course.getId());
        intent.putExtra("NAME", course.getCourseName());
        Log.d("courseID" , course.getId());
        startActivity(intent);





    }

    public class PostAsync extends AsyncTask<String, String, JSONObject> {
        JSONParser jsonParser = new JSONParser();
        private ProgressDialog pDialog;
        private static final String LOGIN_URL = "http://172.20.10.6/~adimaxlee/getCourse.php";

        private static final String TAG_SUCCESS = "success";
        private static final String TAG_MESSAGE = "message";

        @Override
        protected void onPreExecute(){
            pDialog = new ProgressDialog(getActivity());
            pDialog.setTitle("Getting data from server...");
          pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        @Override
        protected JSONObject doInBackground(String... args) {
            try{
                HashMap<String, String> params = new HashMap<>();
                params.put("name", args[0]);
                params.put("password", args[1]);
                Log.d("Request", "Starting");

                JSONObject json = jsonParser.makeHttpRequest(LOGIN_URL, "POST", params);
                if(json!=null){

                    try{

                        user = json.getJSONArray("user");

                        for(int i=0; i<=13; i++) {

                            JSONObject c = user.getJSONObject(i);

                            String id = c.getString("id");
                            String name = c.getString("name");

                             courseID.add(id);
                            courseName.add(name);
                            String imgUrl = c.getString("url");

                         //   int img = getContext().getResources().getIdentifier("c_"+String.valueOf(i+1), "drawable", getContext().getPackageName());

                            //get images from url using bitm
                            Bitmap bmp;
                                        InputStream in = new URL(imgUrl).openStream();
                                        bmp = BitmapFactory.decodeStream(in);
                                       // imageView.setImageBitmap(bmp);

                            courses.add(new Course(courseID.get(i).toString(), courseName.get(i).toString(), bmp));


                            Log.d("courseID", courseID.get(i).toString());
                            Log.d("URL", imgUrl);


                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                  //  Log.d("JSON result", json.toString());
                    return json;

                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject json){
            Log.d("onPostExec", "It works!");
            if(pDialog!=null && pDialog.isShowing()){
                pDialog.dismiss();
            }


            if(json!=null){
                Log.d("courseList", "Are we created?");
                adapter.appendToList(courses);
                adapter.notifyDataSetChanged();


            }

        }

    }




}