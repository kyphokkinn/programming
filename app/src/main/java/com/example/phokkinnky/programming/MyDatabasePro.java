package com.example.phokkinnky.programming;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by phokkinnky on 2/21/2017.
 */

public class MyDatabasePro extends SQLiteAssetHelper {
    private static final String DATABASE_NAME = "DBProgramming.db";
    private static final int DATABASE_VERSION=1;

    public MyDatabasePro(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
