package com.example.phokkinnky.programming;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by phokkinnky on 2/5/2017.
 */
public class FragmentThree extends Fragment {
    @BindView(R.id.recyclerview_history)
    RecyclerView recyclerView;
    private View view;
    private LessonAdapter lessonAdapter;
    private MyDatabasePro myDatabasePro;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_history,container,false);
        ButterKnife.bind(this,view);
        myDatabasePro = new MyDatabasePro(getContext());
        Cursor cursor = myDatabasePro.getReadableDatabase().rawQuery("SELECT * FROM tbhistory WHERE HIS = 1",null);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),1));
        lessonAdapter = new LessonAdapter(cursor,getContext());
        recyclerView.setAdapter(lessonAdapter);
        lessonAdapter.setLessonAdapterLisenter(new LessonAdapterLisenter() {
            @Override
            public void onLessonClick(Lesson lesson) {
                Intent intent = new Intent(getContext(),DetailsActivity.class);
                intent.putExtra("ID",lesson.getLessonID());
                intent.putExtra("TITLE",lesson.getTitlelesson());
                intent.putExtra("LIKEORHIS",lesson.isLike());
                startActivity(intent);
            }
        });
        return view;
    }

}
