package com.example.phokkinnky.programming;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_details);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setCustomView(R.layout.action_bar_lesson);
        TextView textView = (TextView) findViewById(R.id.title_actionbar);

        String chId = getIntent().getStringExtra("ID");
        String chName = getIntent().getStringExtra("TITLE");
        String courseName = getIntent().getStringExtra("courseName");
        String[] folder = chId.split("_");
        textView.setText(courseName +" : "+chName);


        DetailsActivity.this.setTitle(courseName + " : "+chName);


        String url = "http://172.20.10.6/~adimaxlee/"+folder[0]+"/"+chId+".html";

        WebView myWebView = (WebView) findViewById(R.id.webview);
        myWebView.loadUrl(url);


       //TextView textView = (TextView) findViewById(R.id.textView);
        //textView.setText(name );
    }
}
