package com.example.phokkinnky.programming;

/**
 * Created by phokkinnky on 2/5/2017.
 */

public class Lesson {
    public String lessonID;
    public String titlelesson;
    public int like;

    public Lesson(){}
    public String getLessonID() {
        return lessonID;
    }

    public Lesson(String lessonID, String titlelesson, int like) {

        this.lessonID = lessonID;
        this.titlelesson = titlelesson;
        this.like = like;
    }

    public String getTitlelesson() {
        return titlelesson;
    }

    public int isLike() {
        return like;
    }
}
