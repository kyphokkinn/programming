package com.example.phokkinnky.programming;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by phokkinnky on 1/21/2017.
 */

public class CouresAdapter extends RecyclerView.Adapter<CouresAdapter.MyViewHolder> {

    private List<Course> courses = new LinkedList<>();
    private Context context;
    private CouresAdapterLinstener linstener;

    public CouresAdapter(Context context, CouresAdapterLinstener linstener) {
        this.context = context;
        this.linstener = linstener;
    }
    public void appendToList(List<Course> list) {
        if (list == null) {
            return;
        }
        courses.addAll(list);
    }

    public void clear() {
        courses.clear();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder
    {
        public ImageView imageView;

        public MyViewHolder(final View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imagecourse);
        }
    }
    @Override
    public CouresAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View courseview = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_main_course,parent,false);
        return new MyViewHolder(courseview);
    }

    @Override
    public void onBindViewHolder(CouresAdapter.MyViewHolder holder, final int position) {
        holder.imageView.setImageBitmap(courses.get(position).getUrlImage());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linstener.onCourseClick(courses.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return courses.size();
    }
}
