package com.example.phokkinnky.programming;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by phokkinnky on 2/12/2017.
 */

public class LessonActivity extends AppCompatActivity {
    private ArrayList<Lesson> lessonList;
    private LessonAdapter2 lessonAdapter2;
    private LessonAdapterLisenter lessonAdapterLisenter;
    String id, name ;
    private  JSONArray user1;

   private ArrayList<String> chapterID;
    private ArrayList<String> chapterName;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson);
        chapterID = new ArrayList<>();chapterName = new ArrayList<>();

        id = getIntent().getStringExtra("ID");
        name = getIntent().getStringExtra("NAME");
        new GetAsync().execute(id, "test");
        LessonActivity.this.setTitle(name);

        lessonList = new ArrayList<>();
         recyclerView = (RecyclerView) findViewById(R.id.recyclerview_lessonActivity);


    }

    @Override
    protected void onStart(){
        super.onStart();

    }






    private List<Lesson> getDummyData() {
        Database database = new Database(getApplicationContext());
        Log.d("GetDummydata", "WE created before lessonList");
       for (int i = 0; i<=chapterID.size()-1;i++) {
           Log.d("argument recieved", chapterID.get(i).toString());
           Lesson lesson = new Lesson();
           lesson.lessonID=chapterID.get(i).toString();
           lesson.titlelesson=chapterName.get(i).toString();
           lesson.like=0;
           database.AddFav(lesson);
           lessonList.add(lesson);
        }
        return lessonList;
    }



    class GetAsync extends AsyncTask<String, String, JSONObject> {

        JSONParser jsonParser = new JSONParser();

        private ProgressDialog pDialog;

        private static final String LOGIN_URL = "http://172.20.10.6/~adimaxlee/testGet.php";

        @Override
        protected void onPreExecute() {
            pDialog = new ProgressDialog(LessonActivity.this);
            pDialog.setMessage("Attempting login...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... args) {

            try {

                HashMap<String, String> params = new HashMap<>();
                params.put("name", args[0]);
                params.put("password", args[1]);

                Log.d("request", "starting");

                JSONObject json = jsonParser.makeHttpRequest(
                        LOGIN_URL, "GET", params);

                if (json != null) {

                    Log.d("JSON result", json.toString());
                    try {
                        user1 = json.getJSONArray("user");
                        for(int i =0; i<=user1.length()-1; i++) {
                            JSONObject c = user1.getJSONObject(i);
                            String chid = c.getString("id");
                            String chname = c.getString("name");
                            chapterID.add(chid);
                            chapterName.add(chname);

                            Log.d("Json 1st Object", chapterID.get(i).toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    return json;

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {

            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
            }
            if (json != null) {
                Log.d("onPost", "It works!");
                recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(),1));
                recyclerView.setAdapter(new LessonAdapter2(getDummyData(),new LessonAdapter2.onItemClickListener(){
                    @Override
                    public void onLessonClick(Lesson lesson) {
                        MyDatabasePro myDatabasePro = new MyDatabasePro(getApplicationContext());
                        myDatabasePro.getWritableDatabase()
                                .rawQuery("INSERT INTO tbhistory VALUES(" +
                                        lesson.getLessonID()+","+
                                        lesson.getTitlelesson()+",1"+
                                        ")",null);

                        Intent intent = new Intent(getApplicationContext(),DetailsActivity.class);
                        intent.putExtra("ID", lesson.getLessonID());
                        intent.putExtra("TITLE", lesson.getTitlelesson());
                        intent.putExtra("courseName", name);
                        startActivity(intent);

                    }
                },getApplicationContext()));


                //  Toast.makeText(getContext(), json.toString(),
                //   Toast.LENGTH_SHORT).show();

            /*    try {
                    user1 = json.getJSONArray("user");
                    for(int i =0; i<=user1.length()-1; i++) {
                    JSONObject c = user1.getJSONObject(i);
                    String chid = c.getString("id");
                    String chname = c.getString("name");
                    chapterID.add(chid);
                    chapterName.add(chname);

                  Log.d("Json 1st Object", chapterID.get(i).toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } */
            }

        }

    }






}
